"""M3C 2017 Homework 3 solution
"""
import numpy as np
import matplotlib.pyplot as plt
from m1 import hw3 #assumes hw3_template.f90 has been compiled with f2py to generate m1.so

def rwnet(H,n,dim=2,debug=False):
    """
    Simulate random network model
    Input variables
    H: Height at which nodes are initially introduced
    n: Total number of nodes
    dim: dimension of node coordinates
    Output variables
    X: Final node coordinates
    output: a tuple containing any other information you would
    like the function to return, may be left empty
    """
    output=()
    dstar = 1
    X = np.zeros((n,dim))

    for i in range(1,n):
        XN = np.squeeze(X[:i,:]) # current nodes
        xx = np.zeros(dim) # location of new node
        xx[-1] = H

        deltaX = np.max(np.abs(XN-np.outer(np.ones(i),xx)),axis=1)
        dist = np.min(deltaX) #"distance" to nearest node

        #Compute trajectory of node, stopping when appropriate
        while xx[-1]>0 and dist>dstar:
            R = np.random.randint(0,2,dim)
            R[0:-1] = R[0:-1]*2-1
            R[-1] = -R[-1]
            xx = xx + R
            deltaX = np.max(np.abs(XN-np.outer(np.ones(i),xx)),axis=1)
            dist = np.min(deltaX)

        X[i,:] = xx

        if debug:
            if np.mod(i,100)==0:
                print("i,xx,yy=",i,xx)

    return X,output

def performance1():
    """Analyze performance of python and serial fortran codes
    Modify input/output as needed"""

    output = ()
    return output


def performance2():
    """Analyze performance of parallel m-simulation fortran code
    Modify input/output as needed
    """

    output = ()
    return output

def analyze1():
    """Analyze dependence of network height on number of nodes
    Modify input/output as needed
    """
    output = ()
    return output


def analyze2():
    """ Estimate dimensions of simulated networks with d=2,3
        Output: estimated dimensions
    """

    output=()
    return output


def visualize():
    """Generate an animation illustrating the generation of a network
    """

    return None



if __name__ == '__main__':
    #The code here should call the performance and analyze routines above and generate the
    #figures that you are submitting with your codes
    outp1 = performance1()
