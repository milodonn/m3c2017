module part1_mpi
  use cost
  implicit none
  integer :: itermax = 100 !maximum number of iterations used by an optimizer
  real(kind=8) :: tol=1.0e-6 !stopping criteria for optimizer
contains

  subroutine bdglobal_mpi(comm,numprocs,xguess,nb,d,alpha,xf,jf)
    !Use global bracket descent method to minimize cost function, costj
    !input: xguess -- initial guess for location of minimum
    !nb: use nb * nb triangles
    !d: grid spacing for initial nb x nb grid of triangles
    !alpha: agressiveness in centroid shift
    !output: xf -- computed location of minimum, jf -- computed minimum
    !Assumes size(xguess) = 2
    use mpi
    implicit none
    real(kind=8), dimension(2), intent(in) :: xguess
    integer, intent(in) :: comm,numprocs,nb
    real(kind=8), intent(in) :: d, alpha
    real(kind=8), intent(out) :: xf(2),jf !location of minimum, minimum cost
    integer :: myid, ierr
    integer :: istart,iend, nlocal
    integer :: i1,j1,k1,l1
    real(kind=8), allocatable, dimension(:,:) :: xg


  call MPI_COMM_RANK(comm, myid, ierr) !use comm instead of MPI_COMM_WORLD

  !Create decomposition and initial condition
  call mpe_decomp1d(nb*nb,numprocs,myid,istart,iend)
  nlocal = iend - istart + 1
  allocate(xg(2,nlocal))
    l1 = 0
    k1 = 0
    do i1 = 1,nb
      do j1=1,nb
        k1 = k1 + 1
        if (k1>=istart .and. k1<=iend) then !check if triangle i1,j1 "belongs" to this process
          l1 = l1 +1
          xg(1,l1) = (j1-1)*d - (nb-1)*d/2 + xguess(1)
          xg(2,l1) = (i1-1)*d - (nb-1)*d/2 + xguess(2)
        end if
      end do
    end do

end subroutine bdglobal_mpi

!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D

end module part1_mpi

program p12
  !main program can be used to call bdglobal
  use mpi
  use part1_mpi
  implicit none
  integer :: nb
  integer :: myid,ierr,numprocs
  real(kind=8) :: xguess(2),d,alpha
  real(kind=8) :: xf(2),jf

  ! Initialize MPI
     call MPI_INIT(ierr)
     call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)

     open(unit=11,file='data.in')
     read(11,*) xguess(1)
     read(11,*) xguess(2)
     read(11,*) nb
     read(11,*) d
     read(11,*) alpha
     close(11)

     call bdglobal_mpi(MPI_COMM_WORLD,numprocs,xguess,nb,d,alpha,xf,jf)

     !output xf jf to file
     call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
     if (myid==0) then
       open(unit=12,file='p12.dat')
       write(12,*) xf(1)
       write(12,*) xf(2)
       write(12,*) jf
       close(12)
     end if

     call MPI_FINALIZE(ierr)


end program p12
